﻿using GestiuneFilme.Domain.Abstract;
using GestiuneFilme.Domain.Entitities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestiuneFilme.WebUI.Service
{
    public class Notification : Observer
    {
        public void update(string numeFilm, string type, User user)
        {
            System.Diagnostics.Debug.WriteLine("Angajatul " + user.nume + " a fost notificat ca filmul " + numeFilm + " s-a " + type);
        }

    }
}