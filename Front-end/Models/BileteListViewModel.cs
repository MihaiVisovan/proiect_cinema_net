﻿using GestiuneFilme.Domain.Entitities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestiuneFilme.WebUI.Models
{
    public class BileteListViewModel
    {
        public IEnumerable<Bilet> Bilete { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}