﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestiuneFilme.WebUI.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Introduceti un nume")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Introduceti o parola")]
        [StringLength(50, MinimumLength = 4)]
        public string Password { get; set; }

    }
}