﻿using GestiuneFilme.Domain.Abstract;
using GestiuneFilme.Domain.Entitities;
using GestiuneFilme.WebUI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace GestiuneFilme.WebUI.Controllers
{
    [Authorize(Users = "MihaiV, SaraP")]
    public class AdminController : Controller
    {
        private IFilmRepository repositoryFilm;
        private IUserRepository repositoryUser;
        int PageSize = 3;
        Observable observableFilm;
        List<User> users;

        public AdminController(IFilmRepository repoFilm, IUserRepository repoUser)
        {
            repositoryFilm = repoFilm;
            repositoryUser = repoUser;
        }
        public ActionResult Index()
        {
            return View(repositoryFilm.Films);
        }

        public ViewResult Create()
        {
            return View(new Film());
        }
        [HttpPost]
        public ActionResult Create(Film film, HttpPostedFileBase image1)
        {

        
            if (ModelState.IsValid)
            {
                if (image1 != null)
                {
                    film.imageFilm = new byte[image1.ContentLength];
                    image1.InputStream.Read(film.imageFilm, 0, image1.ContentLength);
                }
    
                repositoryFilm.SaveFilm(film);
        
                if (film.id != -1)
                {
                    
                    observableFilm = new Film();
                    users = repositoryUser.Users.Where(r => r.rol == "angajat").ToList();
                    foreach (Observer o in users)
                        observableFilm.attachObserver(o);

                    observableFilm.notifyObserver(film.titlu, "adaugat", users);

                   

                    TempData["message"] = string.Format("{0} has been saved", film.titlu);
                }
                else
                { 
                    TempData["message"] = string.Format("{0} has not been saved", film.titlu);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return View(film);
            }
        }
        public ViewResult Edit(int id)
        {
            Film film = repositoryFilm.Films.FirstOrDefault(f => f.id == id);

            return View(film);
        }

        [HttpPost]
        public ActionResult Edit(Film film)
        {
            if (ModelState.IsValid)
            {
                repositoryFilm.SaveFilm(film);
                if (film.id != -1)
                {
                    observableFilm = new Film();
                    users = repositoryUser.Users.Where(r => r.rol == "angajat").ToList();

                    foreach (Observer o in users)
                        observableFilm.attachObserver(o);

                    observableFilm.notifyObserver(film.titlu, "editat", users);
                    TempData["message"] = string.Format("{0} has been saved", film.titlu);
                }
                else
                {
                    TempData["message"] = string.Format("{0} has not been saved", film.titlu);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return View(film);
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            Film deletedFilm = repositoryFilm.DeleteFilm(id);
            if (deletedFilm != null)
            {
                observableFilm = new Film();
                users = repositoryUser.Users.Where(r => r.rol == "angajat").ToList();

                foreach (Observer o in users)
                    observableFilm.attachObserver(o);

                    observableFilm.notifyObserver(deletedFilm.titlu, "sters", users);

                TempData["message"] = string.Format("{0} was deleted", deletedFilm.titlu);
            }
            return RedirectToAction("Index");
        }

        public ViewResult ListFilme(int page = 1)
        {

            FilmsListViewModel model = new FilmsListViewModel
            {
                Films = repositoryFilm.Films
                        .OrderBy(f => f.id)
                        .Skip((page - 1) * PageSize)
                        .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = repositoryFilm.Films.Count()
                }
            };

            return View(model);
        }

        //Creare Angajat
        public ActionResult IndexAngajat()
        {
            return View(repositoryUser.Users.Where(u => u.rol == "angajat").OrderBy(u => u.nume));
        }

        public ViewResult CreateAngajat()
        {
            return View(new User());
        }
        [HttpPost]
        public ActionResult CreateAngajat(User user)
        {
            if (ModelState.IsValid)
            {
                repositoryUser.SaveUser(user);
                if (user.rol != "bad")
                {
                    TempData["message"] = string.Format("{0} has been saved", user.nume);
                }

                else
                {
                    TempData["message"] = string.Format("{0} has not been saved", user.nume);

                }

                return RedirectToAction("IndexAngajat");
            }
            else
            {
                return View(user);
            }
        }


    }
}