﻿using GestiuneFilme.Domain.Abstract;
using GestiuneFilme.Domain.Entitities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestiuneFilme.WebUI.Controllers.Api
{
    public class FilmsController : ApiController
    {

        private IFilmRepository repository;
        public FilmsController(IFilmRepository repo)
        {
            repository = repo;

        }


        public IEnumerable<Film> GetAllFilms()
        {
            return repository.Films.OrderBy(f => f.titlu);
        }

        public IHttpActionResult GetFilm(int id)
        {
            var film = repository.Films.FirstOrDefault(f => f.id == id);
            if (film == null)
            {
                return NotFound();
            }
            return Ok(film);
        }
    }
}
