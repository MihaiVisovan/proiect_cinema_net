﻿using GestiuneFilme.Domain.Abstract;
using GestiuneFilme.Domain.Entitities;
using GestiuneFilme.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestiuneFilme.WebUI.Controllers
{
    public class CumparatorController : Controller
    {

        private IFilmRepository repositoryFilm;
        private IBiletRepository repositoryBilet;
        int PageSize = 3;

        public CumparatorController(IFilmRepository repoFilm, IBiletRepository repoBilet)
        {
            repositoryFilm = repoFilm;
            repositoryBilet = repoBilet;
        }

        public ViewResult RezervareBilete()
        {
            return View(new Bilet());
        }
        [HttpPost]
        public ActionResult RezervareBilete(Bilet bilet, string titluFilm)
        {
            if (ModelState.IsValid)
            {
                repositoryBilet.SaveBilet(bilet);
                if (bilet.bileteCumparate == -1)
                {
                    TempData["message"] = string.Format("Rezervarea filmului {0} nu a avut loc cu succes", bilet.film);
                }
                else if (bilet.bileteCumparate == 0)
                {
                    TempData["message"] = string.Format("Data filmului {0} nu este corecta", bilet.film);

                }
                else
                {
                    TempData["message"] = string.Format("Rezervarea filmului {0} a avut loc cu succes", bilet.film);

                }
                return RedirectToAction("RezervareBilete");
            }
            else
            {

                FilmsListViewModel model = new FilmsListViewModel
                {
                    Films = repositoryFilm.Films
                       .Where(f => f.titlu == titluFilm)
                 };

                return View(Tuple.Create(bilet, model));
            }
        }

        public ActionResult Search(String titluFilm)
        {
            int page = 1;
            FilmsListViewModel model;
            if (titluFilm != "")
            {
                model = new FilmsListViewModel
                {
                    Films = repositoryFilm.Films
                        .Where(f => f.titlu.Contains(titluFilm))
                        .Skip((page - 1) * PageSize)
                        .Take(PageSize),
                    PagingInfo = new PagingInfo
                    {
                        CurrentPage = page,
                        ItemsPerPage = PageSize,
                        TotalItems = repositoryFilm.Films.Count()
                    }
                };

            }
            else
            {
                model = new FilmsListViewModel
                {
                    Films = repositoryFilm.Films
                       .OrderBy(f => f.id)
                       .Skip((page - 1) * PageSize)
                       .Take(PageSize),
                    PagingInfo = new PagingInfo
                    {
                        CurrentPage = page,
                        ItemsPerPage = PageSize,
                        TotalItems = repositoryFilm.Films.Count()
                    }
                };


            }
            return View(model);
        }


        public ViewResult ListFilme(int page = 1)
        {

            FilmsListViewModel model = new FilmsListViewModel
            {
                Films = repositoryFilm.Films
                        .OrderBy(f => f.id)
                        .Skip((page - 1) * PageSize)
                        .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = repositoryFilm.Films.Count()
                }
            };

            return View(model);
        }
    }
}