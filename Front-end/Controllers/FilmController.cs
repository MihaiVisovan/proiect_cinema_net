﻿using GestiuneFilme.Domain.Abstract;
using GestiuneFilme.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestiuneFilme.WebUI.Controllers
{
    [Authorize]
    public class FilmController : Controller
    {
        private readonly IFilmRepository repository;
        public int PageSize = 3;
        public FilmController (IFilmRepository repo)
        {
            repository = repo;

        }

        public ViewResult List(int page = 1)
        {

            FilmsListViewModel model = new FilmsListViewModel
            {
                Films = repository.Films
                        .OrderBy(f => f.id)
                        .Skip((page - 1) * PageSize)
                        .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.Films.Count()
                }
            };

            return View(model);
         }
    }
}