﻿using GestiuneFilme.Domain.Abstract;
using GestiuneFilme.Domain.Entitities;
using GestiuneFilme.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace GestiuneFilme.WebUI.Controllers
{
    [Authorize]
    public class AngajatController : Controller
    {

        private IBiletRepository repository;
        int PageSize = 3;
        public AngajatController(IBiletRepository repo)
        {
            repository = repo;
        }
        public ActionResult Index()
        {
            return View(repository.Bilete);
        }

        public ViewResult Create()
        {
            return View(new Bilet());
        }
        [HttpPost]
        public ActionResult Create(Bilet bilet)
        {
            if (ModelState.IsValid)
            {
                repository.SaveBilet(bilet);
                if (bilet.bileteCumparate == -1)
                {
                    TempData["message"] = string.Format("Rezervarea filmului {0} nu a avut loc cu succes", bilet.film);
                }
                else if(bilet.bileteCumparate == 0)
                {
                    TempData["message"] = string.Format("Data filmului {0} nu este corecta", bilet.film);

                }
                else
                {
                    TempData["message"] = string.Format("Rezervarea filmului {0} a avut loc cu succes", bilet.film);

                }
                return RedirectToAction("Create");
            }
            else
            {
                return View(bilet);
            }
        }
    


        public ViewResult ListBilete(int page = 1)
        {

            BileteListViewModel model = new BileteListViewModel
            {
                Bilete = repository.Bilete
                        .OrderBy(f => f.id)
                        .Skip((page - 1) * PageSize)
                        .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.Bilete.Count()
                }
            };

            return View(model);
        }










    }
}