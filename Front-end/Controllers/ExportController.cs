﻿using GestiuneFilme.Domain.Abstract;
using GestiuneFilme.Domain.Concrete;
using GestiuneFilme.Domain.Entitities;
using GestiuneFilme.WebUI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestiuneFilme.WebUI.Controllers
{
    [Authorize]
    public class ExportController : Controller
    {
        // GET: Export
        private IFilmRepository repositoryFilm;
        private IBiletRepository repositoryBilet;


        public ExportController(IFilmRepository repoFilm, IBiletRepository repoBilet)
        {
            repositoryFilm = repoFilm;
            repositoryBilet = repoBilet;
        }



        public ActionResult Download(string titlu, string exportType)
        {

            Exporter exporter = ExportFactory.chooseType(exportType);

            List<Bilet> bilete = repositoryBilet.Bilete.Where(b => b.film == titlu).ToList();


            return File(exporter.exportFilms(bilete), "text/plain", "file.txt");

        }
    }
}