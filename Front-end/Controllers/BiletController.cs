﻿using GestiuneFilme.Domain.Abstract;
using GestiuneFilme.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestiuneFilme.WebUI.Controllers
{
    [Authorize]
    public class BiletController : Controller
    {
        private readonly IBiletRepository repository;
        public int PageSize = 3;
        public BiletController(IBiletRepository repo)
        {
            repository = repo;

        }

        public ViewResult List(int page = 1)
        {

            BileteListViewModel model = new BileteListViewModel
            {
                Bilete = repository.Bilete
                        .OrderBy(f => f.id)
                        .Skip((page - 1) * PageSize)
                        .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.Bilete.Count()
                }
            };

            return View(model);
        }
    }
}