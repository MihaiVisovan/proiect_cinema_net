﻿using GestiuneFilme.Domain.Abstract;
using GestiuneFilme.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace GestiuneFilme.WebUI.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {

        IAuthentication authentication;
        public AccountController(IAuthentication authentication)
        {
            this.authentication = authentication;
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {

            if (ModelState.IsValid)
            {

                if (authentication.Authenticate(model.UserName, model.Password) == "admin")
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false);
                    return Redirect(returnUrl ?? Url.Action("ListFilme", "Admin"));
                }
                if(authentication.Authenticate(model.UserName, model.Password) == "angajat")
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false);
                    return Redirect(returnUrl ?? Url.Action("ListBilete", "Angajat"));
                }
                if (authentication.Authenticate(model.UserName, model.Password) == "cumparator")
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false);
                    return Redirect(returnUrl ?? Url.Action("ListFilme", "Cumparator"));
                }
                else
                {
                    ModelState.AddModelError("", "Incorrect username or password");
                    return View();
                }

            }
            else
            {
                return View();

            }
        }
        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Login", "Account");

        }


    }
}