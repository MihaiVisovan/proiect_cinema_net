﻿using GestiuneFilme.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneFilme.Domain.Entitities
{
    public class User : Observer
    {
        [Required(ErrorMessage = "Introduceti numele")]
        public string nume { get; set; }
        [Required(ErrorMessage = "Introduceti username-ul")]
        [Key]
        public string username { get; set; }
        [Required(ErrorMessage = "Introduceti parola")]
        public string parola { get; set; }
        public string rol { get; set; }

     

        public void update(string numeFilm, string type, User user)
        {
            System.Diagnostics.Debug.WriteLine("Angajatul " + user.nume + " a fost notificat ca filmul " + numeFilm + " s-a " + type);

        }
    }
}
