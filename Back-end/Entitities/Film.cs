﻿using GestiuneFilme.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneFilme.Domain.Entitities
{
    public class Film : Observable
    {
        [Required(ErrorMessage = "Introduceti titlu")]
        public String titlu { get; set; }

        [Required(ErrorMessage = "Introduceti regia")]
        public String regia { get; set; }

        [Required(ErrorMessage = "Introduceti distributia")]
        public String distributia { get; set; }

        [Required]
        [Range(0.01, double.MaxValue, ErrorMessage = "Trebuie introdus un pret pozitiv")]
        public int numarBilete { get; set; }

        [Required(ErrorMessage = "Introduceti data premierei")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime dataPremiera { get; set; }

        [Required(ErrorMessage = "Introduceti pana cand va fi difuzat filmul")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime dataDifuzare { get; set; }

        [Required(ErrorMessage = "Introduceti ora la care va fi difuzat filmul")]
        public DateTime oraZilnica;

        public byte[] imageFilm { get; set; }

        public int id { get; set; }
   


                
        private List<Observer> observers = new List<Observer>();


        public void attachObserver(Observer o)
        {
            observers.Add(o);
        }

        public void detachObserver(Observer o)
        {
            observers.Remove(o);
        }

        public void notifyObserver(string numeFilm, string type, List<User> users)
        {
            foreach (var o in observers)
            {
                o.update(numeFilm, type, users.First());
                users.Remove(users.First());
            }
        }
    }
}
