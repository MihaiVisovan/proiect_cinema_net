﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneFilme.Domain.Entitities
{
    public class Bilet
    {
        [Required(ErrorMessage = "Introduceti filmul")]
        public String film { get; set; }
        [Required(ErrorMessage = "Introduceti randul")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Trebuie introdus un rand pozitiv")]
        public int rand { get; set; }
        [Required(ErrorMessage = "Introduceti locul")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Trebuie introdus un loc pozitiv")]
        public int loc { get; set; }
        [Required(ErrorMessage = "Introduceti data")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime data{ get; set; }
        public int bileteCumparate { get; set; }
        public int id { get; set; }

    }
}
