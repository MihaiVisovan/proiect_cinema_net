﻿using GestiuneFilme.Domain.Entitities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneFilme.Domain.Abstract
{
    public interface IBiletRepository
    {
        IEnumerable<Bilet> Bilete { get; }

        void SaveBilet(Bilet bilet);


    }
}
