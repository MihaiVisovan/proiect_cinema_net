﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneFilme.Domain.Abstract
{
   public interface IAuthentication
    {
        string Authenticate(string username, string parola);
        bool Logout();
    }
}
