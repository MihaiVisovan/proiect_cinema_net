﻿using GestiuneFilme.Domain.Entitities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneFilme.Domain.Abstract
{
    public interface Observable
    {
        void attachObserver(Observer o);
        void detachObserver(Observer o);
        void notifyObserver(String film, String titlu, List<User> users);

    }
}
