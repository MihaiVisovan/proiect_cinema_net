﻿using GestiuneFilme.Domain.Entitities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneFilme.Domain.Abstract
{
    public interface Exporter
    {
        byte[] exportFilms(List<Bilet> bilete);

    }
}
