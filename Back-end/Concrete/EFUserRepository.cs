﻿using GestiuneFilme.Domain.Abstract;
using GestiuneFilme.Domain.Entitities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneFilme.Domain.Concrete
{
    public class EFUserRepository : IUserRepository
    {

        private readonly EFDbContext context = new EFDbContext();
        public IEnumerable<User> Users
        {
            get
            {
                return context.Users;
            }
        }
        public void SaveUser(User user)
        {
            User dbEntry = context.Users.Find(user.username);
            String cryptedPass = getMd5Hash(user.parola);
            if (dbEntry == null)
            {
                user.parola = cryptedPass;
                user.rol = "angajat";
                context.Users.Add(user);
            }
            else
            {
                user.rol = "bad";
            }
            context.SaveChanges();
        }

        public string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
    }
}
