﻿using GestiuneFilme.Domain.Abstract;
using GestiuneFilme.Domain.Entitities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneFilme.Domain.Concrete
{
    public class EFBiletRepository : IBiletRepository
    {

        private readonly EFDbContext context = new EFDbContext();
        public IEnumerable<Bilet> Bilete
        {
            get
            {
                return context.Bilete;
            }   
        }

        public void SaveBilet(Bilet bilet)
        {
            //daca e ocupat deja acel loc, nu introducem biletul respectiv 
            Bilet dbEntry = context.Bilete.FirstOrDefault(b => b.film == bilet.film && b.rand == bilet.rand && b.loc == bilet.loc && b.data == bilet.data);

            //cautam sa vedem daca exista filmul respectiv
            Film film = context.Films.FirstOrDefault(f => f.titlu == bilet.film);

            //verificam ca data in care vrem biletul sa fie intre dataPremiera <-> dataDifuzare
            if (dbEntry == null && film != null) {

                DateTime finalDate = bilet.data.AddMinutes(bilet.data.Minute * -1).AddSeconds(bilet.data.Second * -1);

                int checkDate = DateTime.Compare(film.dataPremiera, finalDate);
                int checkDate2 = DateTime.Compare(film.dataDifuzare, finalDate);

                //verificam ca nr bilete cumparate < bilete puse in vanzare
                int? numarBileteInt;
                numarBileteInt = context.Bilete.Where(b => b.film == bilet.film && b.data == bilet.data).Sum(x => (int?)x.bileteCumparate) ?? 0;


                if (film.numarBilete > numarBileteInt && checkDate < 0 && checkDate2 >= 0)
                {
                    bilet.data = finalDate;
                    bilet.bileteCumparate = 1;
                    context.Bilete.Add(bilet);

                }
                else
                {
                    bilet.bileteCumparate = -1;
                }
            }

            context.SaveChanges();
        }
    }
}
