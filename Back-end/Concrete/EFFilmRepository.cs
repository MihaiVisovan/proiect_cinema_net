﻿using GestiuneFilme.Domain.Abstract;
using GestiuneFilme.Domain.Entitities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneFilme.Domain.Concrete
{
    public class EFFilmRepository : IFilmRepository
    {
        private readonly EFDbContext context = new EFDbContext();
 
        public IEnumerable <Film> Films 
        {
            get
            {
                return context.Films;
            }
        }

        public void SaveFilm(Film film)
        {

            int checkDate = DateTime.Compare(film.dataPremiera, film.dataDifuzare);
            if (film.id == 0 && checkDate < 0)
            {
                context.Films.Add(film);
           
            }
            else
            {
                Film dbEntry = context.Films.Find(film.id);
                if (dbEntry != null && checkDate < 0)
                {
                    dbEntry.titlu = film.titlu;
                    dbEntry.regia = film.regia;
                    dbEntry.distributia = film.distributia;
                    dbEntry.numarBilete = film.numarBilete;
                    DateTime finalDateDif = film.dataDifuzare.AddMinutes(film.dataDifuzare.Minute * -1).AddSeconds(film.dataDifuzare.Second * -1);
                    DateTime finalDatePrem = film.dataPremiera.AddMinutes(film.dataPremiera.Minute * -1).AddSeconds(film.dataPremiera.Second * -1);
                    dbEntry.dataPremiera = finalDatePrem;
                    dbEntry.dataDifuzare = finalDateDif;
                    dbEntry.imageFilm = film.imageFilm;

                }
                else
                {
                    film.id = -1;
                }
            }
            context.SaveChanges();
        }


        public Film DeleteFilm(int filmId)
        {
            Film dbEntry = context.Films.Find(filmId);
            if (dbEntry != null)
            {
                context.Films.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
