﻿using GestiuneFilme.Domain.Abstract;
using GestiuneFilme.Domain.Entitities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GestiuneFilme.Domain.Concrete
{
    public class JsonExporter : Exporter
    {
        
        public byte[] exportFilms(List<Bilet> bilete)
        {
            MemoryStream memoryStream = new MemoryStream();
            TextWriter tw = new StreamWriter(memoryStream);

            var json = JsonConvert.SerializeObject(bilete.ToArray());

            tw.WriteLine(json);
            tw.Flush();
            tw.Close();

            return memoryStream.GetBuffer();
        }

    }
}
