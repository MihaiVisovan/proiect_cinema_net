﻿using GestiuneFilme.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneFilme.Domain.Concrete
{
    public class FormsAuthentication : IAuthentication
    {
        private readonly EFDbContext context = new EFDbContext();

        public string Authenticate(string username, string parola)
        {

            String cryptedPass = getMd5Hash(parola);
            var resultAdmin = context.Users.FirstOrDefault(u => u.username == username && u.parola == cryptedPass && u.rol == "admin");
            var resultAngajat = context.Users.FirstOrDefault(u => u.username == username && u.parola == cryptedPass && u.rol == "angajat");
            var resultCumparator = context.Users.FirstOrDefault(u => u.username == username && u.parola == cryptedPass && u.rol == "cumparator");


            if (resultAdmin != null)
            {
                    return "admin";
            }
            if (resultAngajat != null)
            {
                return "angajat";
            }
            if(resultCumparator != null)
            {
                return "cumparator";
            }
            return null;
        }

        public bool Logout()
        {
            return true;
        }

        public string getMd5Hash(string input)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }
    }
}
