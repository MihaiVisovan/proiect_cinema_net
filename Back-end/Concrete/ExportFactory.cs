﻿using GestiuneFilme.Domain.Abstract;
using GestiuneFilme.Domain.Entitities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneFilme.Domain.Concrete
{
    public class ExportFactory : Exporter
    {

        public static Exporter chooseType(string type)
        {
            if (type == "JSON")
            {
                return new JsonExporter();

            }
            else if (type == "CSV")
            {
                return new CsvExporter();
            }
            return null;
        }

        public byte[] exportFilms(List<Bilet> bilete)
        {
            throw new NotImplementedException();
        }
    }
}
