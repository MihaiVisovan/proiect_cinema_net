﻿using CsvHelper;
using GestiuneFilme.Domain.Abstract;
using GestiuneFilme.Domain.Entitities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestiuneFilme.Domain.Concrete
{
    public class CsvExporter : Exporter
    {
        public byte[] exportFilms(List<Bilet> bilete)
        {

            MemoryStream memoryStream = new MemoryStream();
            TextWriter tw = new StreamWriter(memoryStream);

            StringBuilder csv = new StringBuilder();
           
            foreach (Bilet bilet in bilete)
            {
                csv.Append(bilet.film.ToString() + ", ");
                csv.Append(bilet.data.ToString() + ", ");
                csv.Append(bilet.loc.ToString() + ", ");
                csv.Append(bilet.rand.ToString() + ", ");
                csv.Append(bilet.bileteCumparate.ToString());
                csv.Append(System.Environment.NewLine);
            }

            tw.WriteLine(csv);
            tw.Flush();
            tw.Close();

            return memoryStream.GetBuffer();
        }
    }
}
